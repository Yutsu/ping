let form = document.getElementById('form');

form.addEventListener('submit', e => {

    e.preventDefault();
    let email = form['email']; //id = email input
    let emailValue = email.value;
    let smallError = form.querySelector('small');

    if((!emailValue) || !validateEmail(emailValue)) {
        email.classList.add('error'); //.form-input -> input.error <-
        smallError.innerText = "Email invalid or blank";
        smallError.style.display = 'inline-block';
    } else {
        email.classList.remove('error');
        smallError.style.display = 'none';
    }
});

function validateEmail(email) {
    let re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}
